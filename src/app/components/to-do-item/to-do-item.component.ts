import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Todo } from 'src/app/modules/Todo';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-to-do-item',
  templateUrl: './to-do-item.component.html',
  styleUrls: ['./to-do-item.component.css']
})
export class ToDoItemComponent implements OnInit {
  @Input() todo:Todo;
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter();

  constructor(private todoService:TodoService) { }

  ngOnInit() {
  }

  // Set Dynamic Classes
  setClasses(){
    let classes = {
      todo: true,
      'is-complete': this.todo.completed
    }

    return classes;
  }

  // onToggle
  onToggle(todo){
    // toggle in ui
    todo.completed = !todo.completed;

    // togle on server
    this.todoService.toggleCompleted(todo).subscribe(todo => {
      console.log(todo)})
  }

  // onToggle
  onDelete(todo){
    this.deleteTodo.emit(todo)
  }
  
}
